#!/bin/sh

# Run drush as www-data from /srv/www/
command="/srv/www/vendor/bin/drush -r /srv/www/"
for argument in "$@"; do
  command="${command} \"${argument}\""
done
su -s /bin/sh -c "${command}" www-data
