# Configuration for www.

# HTTP server.
server {
    listen 80 default_server; # IPv4
    #listen [fe80::202:b3ff:fe1e:8328]:80 default_server ipv6only=on; # IPv6

    server_name _;
    limit_conn arbeit 32;

    # Access and error logs.
    access_log /var/log/www/access.log main;
    error_log /var/log/www/error.log;

    # Redirect all requests to https, except localhost.
    #set $https_redirect '';
    #if ($remote_addr != '127.0.0.1') {
    #  set $https_redirect '${https_redirect}R';
    #}
    #if ($http_x_forwarded_proto != 'https') {
    #  set $https_redirect '${https_redirect}N';
    #}
    #if ($https_redirect = RN) {
    #  return 301 https://$host$request_uri;
    #}

    # See the blacklist.conf file at the parent dir: /etc/nginx.
    # Deny access based on the User-Agent header.
    if ($bad_bot) {
        return 444;
    }

    # Deny access based on the Host header.
    if ($bad_host) {
        return 444;
    }

    # Protection against illegal HTTP methods. Out of the box only HEAD,
    # GET and POST are allowed.
    if ($not_allowed_method) {
        return 405;
    }

    # Filesystem root of the site and index.
    root /srv/www/web;
    index index.php;

    # If you're using a Nginx version greater or equal to 1.1.4 then
    # you can use keep alive connections to the upstream be it
    # FastCGI or Apache. If that's not the case comment out the line below.
    fastcgi_keep_conn on; # keep alive to the FCGI upstream

    # Uncomment if you're proxying to Apache for handling PHP.
    #proxy_http_version 1.1; # keep alive to the Apache upstream

    # Generic configuration: for most Drupal 7 sites.
    include apps/drupal/drupal.conf;

    # Configuration for Drupal 7 sites to serve URIs that need
    # to be **escaped**
    #include apps/drupal/drupal_escaped.conf;

    # Configuration for Drupal 7 sites that use boost.
    #include apps/drupal/drupal_boost.conf;

    # Configuration for Drupal 7 sites that use boost if having
    # to serve URIs that need to be **escaped**
    #include apps/drupal/drupal_boost_escaped.conf;

    # Configuration for updating the site via update.php and running
    # cron externally. If you don't use drush for running cron use
    # the configuration below.
    #include apps/drupal/drupal_cron_update.conf;

    # Installation handling. This should be commented out after
    # installation if on an already installed site there's no need
    # to touch it. If on a yet to be installed site. Uncomment the
    # line below and comment out after installation. Note that
    # there's a basic auth in front as secondary ligne of defense.
    #include apps/drupal/drupal_install.conf;

    # Support for upload progress bar. Configurations differ for
    # Drupal 6 and Drupal 7.
    include apps/drupal/drupal_upload_progress.conf;

    # Including the php-fpm status and ping pages config.
    # Uncomment to enable if you're running php-fpm.
    #include php_fpm_status_vhost.conf;

    # Including the Nginx stub status page for having stats about
    # Nginx activity: http://wiki.nginx.org/HttpStubStatusModule.
    include nginx_status_vhost.conf;
}
