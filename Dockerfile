FROM alpine:3.13 AS drupal9
ARG S6_OVERLAY_VERSION=v3.1.0.1

# Nginx install
RUN apk add --no-cache nginx

# Nginx config
ADD https://gitlab.com/mustanggb/nginx-config/-/archive/master/nginx-config-master.tar.gz /tmp/
#RUN rm -r /etc/nginx/ && \ # @todo: Add
RUN mv /etc/nginx/ /etc/nginx-original/ && \
    mkdir /etc/nginx/ && \
    tar -xzC /etc/nginx/ --strip-components=1 -f /tmp/nginx-config-master.tar.gz && \
    rm /tmp/nginx-config-master.tar.gz
#RUN mv /etc/nginx/ /etc/nginx-original/ && \
#    git clone --depth=1 https://gitlab.com/mustanggb/nginx-config.git /etc/nginx/ && \
#    rm -R /etc/nginx/.git
ADD www.conf /etc/nginx/sites-available/www
RUN ln -s ../sites-available/www /etc/nginx/sites-enabled/www
RUN mkdir /var/cache/nginx/ /var/log/www/
RUN sed -ri -e 's/php7/php8/' /etc/nginx/upstream_phpcgi_unix.conf

# Packages install
RUN apk add --no-cache nginx-mod-http-upload-progress \
                       nginx-mod-http-headers-more \
                       php8-ctype \
                       php8-curl \
                       php8-dom \
                       php8-fileinfo \
                       php8-fpm \
                       php8-gd \
                       php8-mbstring \
                       php8-opcache \
                       php8-pecl-apcu \
                       php8-pecl-imagick \
                       php8-pdo_mysql \
                       php8-phar \
                       php8-session \
                       php8-simplexml \
                       php8-tokenizer \
                       php8-xml \
                       php8-xmlwriter \
                       php8-zip \
                       postfix \
                       curl \
                       git \
                       mysql-client \
                       openssl \
                       openssh \
                       patch

# System config
RUN adduser -u 82 -g "www-data" -h "/tmp" -s /bin/false -D -H -S -G www-data www-data

# s6 install
# Based on harningt/docker-base-alpine-s6-overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/$S6_OVERLAY_VERSION/s6-overlay-noarch.tar.xz /tmp/
ADD https://github.com/just-containers/s6-overlay/releases/download/$S6_OVERLAY_VERSION/s6-overlay-x86_64.tar.xz /tmp/
RUN tar -xJpC / -f /tmp/s6-overlay-noarch.tar.xz && \
    tar -xJpC / -f /tmp/s6-overlay-x86_64.tar.xz && \
    rm /tmp/s6-overlay-noarch.tar.xz && \
    rm /tmp/s6-overlay-x86_64.tar.xz

# s6 config
COPY init/ /etc/s6-overlay/s6-init.d/
COPY services/ /etc/s6-overlay/s6-rc.d/

# SSH config
#RUN adduser -u 500 -g "remote" -h "/" -s /bin/sh -D -H remote remote
#RUN adduser -u 500 -g "remote" -s /bin/sh -D remote remote
#RUN ssh-keygen -q -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ''
#RUN sed -i "s/#PermitRootLogin.*/PermitRootLogin no/" /etc/ssh/sshd_config
#RUN sed -i "s/#PasswordAuthentication.*/PasswordAuthentication no/" /etc/ssh/sshd_config
#RUN sed -i "s/#ChallengeResponseAuthentication.*/ChallengeResponseAuthentication no/" /etc/ssh/sshd_config
#RUN sed -i "s/AllowTcpForwarding.*/AllowTcpForwarding yes/" /etc/ssh/sshd_config
#RUN echo "AllowUsers remote" >> /etc/ssh/sshd_config
RUN echo "root:root" | chpasswd
RUN ssh-keygen -A
RUN sed -i "s/#PermitRootLogin.*/PermitRootLogin yes/" /etc/ssh/sshd_config

# PHP config
RUN sed -ri -e 's/^;?user\s?=.*$/user = www-data/' /etc/php8/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?group\s?=.*$/group = www-data/' /etc/php8/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?listen\s?=.*$/listen = \/run\/php8-fpm.sock/' /etc/php8/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?listen\.owner\s?=.*$/listen.owner = nginx/' /etc/php8/php-fpm.d/www.conf
RUN sed -ri -e 's/^;?listen\.group\s?=.*$/listen.group = www-data/' /etc/php8/php-fpm.d/www.conf
RUN ln -s /usr/bin/php8 /usr/bin/php

# Postfix config
RUN echo "inet_interfaces = loopback-only" > /etc/postfix/main.cf

# Composer install
ADD https://getcomposer.org/composer-stable.phar /usr/local/bin/composer
RUN chmod +rx /usr/local/bin/composer
ENV COMPOSER_HOME /usr/local/composer

# Cron install
ADD cron.sh /etc/periodic/hourly/drupal
RUN chmod +x /etc/periodic/hourly/drupal

# Drupal install
RUN git clone -b 9.3.9 https://github.com/drupal/recommended-project.git /srv/www/ && \
    composer --no-cache install -d /srv/www/

# Drush install
RUN composer --no-cache require drush/drush -d /srv/www/
ADD drush.sh /usr/local/bin/drush
RUN chmod +rx /usr/local/bin/drush

# Drupal config
RUN chgrp -R www-data /srv/www/web/ && \
    mkdir /srv/www/web/sites/default/files/ && \
    chown www-data:www-data /srv/www/web/sites/default/files/ && \
    cp /srv/www/web/sites/default/default.settings.php /srv/www/web/sites/default/settings.php && \
    chown www-data:www-data /srv/www/web/sites/default/settings.php

# Use multi-stage build to compress layers for a smaller image
FROM scratch
COPY --from=drupal9 / /

# Environmental variables
ENV COMPOSER_HOME /usr/local/composer

# Give init scripts a 5 minute timeout
ENV S6_CMD_WAIT_FOR_SERVICES_MAXTIME=300000

# Terminate if init scripts fail
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2

# Expose ports
EXPOSE 80

# Working directory
WORKDIR /srv/www/

# Run s6
ENTRYPOINT /init
